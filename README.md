# Trabalho Caixeiro Viajante
Disciplina: **Sistemas Distribuidos**

Professor: **Gabriel de Biasi**

----

## Sobre
Este projeto foi implementado usando a linguagem **PHP** afim de simular uma solução
para o problema do _caixeiro viajante_. As classes estão na pasta **classes**
(todas bem comentadas) e foram utilizadas no arquivo **caixeiro-viajante.php** 
para implementação da lógica.

### Clone do projeto
```
git clone https://gitlab.com/moreiraandre/caixeiro-viajante.git
```

### Preparar ambiente
Execute o arquivo **configura-php.sh** para recompilar o PHP 7.2 com a biblioteca
_pthreads_, instalar e fazer as devidas configurações automaticamente.
```
cd caixeiro-viajante
chmod +x configura-php.sh
sudo ./configura-php.sh
```
**Nota**: este projeto foi testado em uma máquina virtual limpa com o Sitema
Operacional _Linux Ubuntu_, a execução do script acima pode demorar um pouco
levando em consideração a velocidade da internet.

### Testar projeto
```
php caixeiro-viajante.php
```

### Conceitos implementados neste trabalho
- [Problema do Caixeiro Viajante](https://pt.wikipedia.org/wiki/Problema_do_caixeiro-viajante)
- Heurística simples de mutação
- [Processos Threads](https://pt.wikipedia.org/wiki/Thread_(computação))