<?php

require 'classes/CaixeiroViajante.php';
require 'classes/Cidade.php';
require 'classes/Viagem.php';

$caixeiro = new CaixeiroViajante($fitness);

system('clear'); // LIMPANDO O CMD

echo "*** TRABALHO CAIXEIRO VIAJANTE ***\n";
echo "==================================\n";

$nIteracoes = readline("INFORME O NÚMERO DE ITERAÇÕES: ");
try {
    $caixeiro->setNIteracoes($nIteracoes);
} catch (Throwable $e) {
    echo "Informação inválida!";
    exit("\n");
}

$sair = false; // FAZ O CONTROLE DO TÉRMINO DE LEITURA DAS CIDADES
echo "\n============== LEITURA DAS CIDADES ===============\n";
echo "INFORME DOIS NÚMEROS INTEIROS SEPARADOS POR ESPAÇO\n";
echo "--------------------------------------------------\n";
do { // ESTRUTURA DE REPETIÇÃO QUE OBRIGA A PRIMEIRA ITERAÇÃO
    $auxCidades = readline("CIDADE " . ($caixeiro->getTotalCidades() + 1) . ": "); // LÊ A CIDADE
    $auxCidades = explode(' ', $auxCidades); // SEPARA O x E y

    if (count($auxCidades) != 2) // CASO O VETOR NÃO CONTENHA DUAS POSIÇÕES
        echo "Informação inválida!\n";
    else {
        if ($auxCidades[0] == 0 && $auxCidades[1] == 0) // VERIFICA SE O USUÁRIO DESEJA TERMINAR A LEITURA
            $sair = true;
        else { // CASO CONTRÁRIO ADICIONA A CIDADE AO VETOR
            try {
                $newCidade = new Cidade(); // CRIA NOVO OBJETO DA CLASSE Cidade
                $newCidade->setX($auxCidades[0]);
                $newCidade->setY($auxCidades[1]);

                $caixeiro->addCidade($newCidade); // ADICIONA A CIDADE A LISTA DE CIDADES DO CaixeiroViajante
            } catch (Throwable $e) {
                echo "Informação inválida!\n";
            }
        }
    }
} while (!$sair); // CASO $sair SEJA VERDADEIRO A LEITURA TERMINA

$caixeiro->calcFitness();
