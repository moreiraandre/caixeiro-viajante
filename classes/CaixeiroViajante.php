<?php

/**
 * Responsável por receber as cidades e iniciar as classes (threads) de cálculo do fitness
 */

class CaixeiroViajante
{
    private
        $vetCidades = [],
        $nIteracoes;

    public function __construct(&$fitness)
    {
        $this->fitness = $fitness;
    }

    /**
     * @return int
     */
    public function getNIteracoes(): int
    {
        return $this->nIteracoes;
    }

    /**
     * @param int $nIteracoes
     */
    public function setNIteracoes(int $nIteracoes)
    {
        $this->nIteracoes = $nIteracoes;
    }

    /**
     * @param Cidade $cidade
     */
    public function addCidade(Cidade $cidade)
    {
        $this->vetCidades[] = $cidade;
    }

    /**
     * @return int
     */
    public function getTotalCidades()
    {
        return count($this->vetCidades);
    }

    /**
     * Cria objetos da classe Viagem para fazer o processamento
     */
    public function calcFitness()
    {
        // CRIANDO 4 THREADS
        for ($x = 0; $x < 4; $x++) {
            $thread = new Viagem($this->vetCidades, $this->getNIteracoes(), $x+1);
            $thread->start();
        }
    }
}