<?php

/**
 * Responsável por guardar o X e Y da cidade bem como fornecer função para calcular a distância em relação a outra cidade
 */

class Cidade
{
    /**
     * Posições da cidade
     */
    private $x, $y;

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @param int $x
     */
    public function setX(int $x)
    {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @param int $y
     */
    public function setY(int $y)
    {
        $this->y = $y;
    }

    /**
     * Retorna a distância entre a cidade atual a a recebida por parametro
     *
     * @param Cidade $cidade Cidade que será usada como referência para fazer o cálculo com a cidade atual
     * @return float
     */
    public function getDistancia(Cidade $cidade): float
    {
        return number_format(
            sqrt( // RAIZ QUADRADA
                (
                    ($this->getX() - $cidade->getX())
                    ** 2 // ** SIGNIFICA EXPONENCIAÇÃO
                )
                +
                (
                    ($this->getY() - $cidade->getY())
                    ** 2
                )
            ),
            3, null, ''); // FORMATANDO COM 3 CASAS DECIMAIS, SEPARADOR ',' PARA
                                                          // DECIMAIS (PRÉ-DEFINIDO) E RETIRANDO SEPARADOR DE MILHAR
    }
}