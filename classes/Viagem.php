<?php

/**
 * Responsável por fazer o calculo do fitness usando thread
 */

class Viagem extends Thread
{
    private
        /**
         * @var array Armazena os objetos da classe Cidade
         */
        $vetCidades,

        /**
         * @var int Armazena a quantidade de interações por thread
         */
        $nInteracoes,

        /**
         * @var array Armazena as distancias entre as cidades
         */
        $vetDistancias = [],

        /**
         * @var int Índice da Thread
         */
        $idx,

        /**
         * @var float Amrmazena o melhor fitness do processo
         */
        $fitness;

    /**
     * Atribuição de variáveis na criação do objeto
     *
     * @param array $vetCidades
     * @param $nInteracoes
     */
    public function __construct(array $vetCidades, int $nInteracoes, $idx)
    {
        $this->vetCidades = $vetCidades;
        $this->vetCidades[] = $this->vetCidades[0]; /* ADICIONA A PRIMEIRA POSIÇÃO NO FIM DO VETOR PARA CONSIDERAR A
                                                       VOLTA PARA CASA*/
        $this->nInteracoes = $nInteracoes;
        $this->idx = $idx;
    }

    /**
     * Essa função é executada ao chamar o método start() da thread
     *
     * Calcula o fitness de acordo com o número de iterações
     */
    public function run()
    {
        for ($i = 0; $i < $this->nInteracoes; $i++) { // QUANTIDADE DE ITERAÇÕES
            $this->mutacao(); // TROCA AS CIDADES DE LUGAR
            //RECEBE A SOMA DE TODAS AS DISTÂNCIAS QUE COMPREENDEM A DISTÂNCIA TOTAL DA VIAGEM EM CADA TROCA
            $this->vetDistancias[] = $this->calcularDistancias();
            // SE O FITNESS NÃO ESTIVER INICIADO ELE RECEBE A PRIMEIRA SOMA DE DISTÂNCIAS PARA INICIAR A COMPARAÇÃO
            if ($this->fitness == null)
                $this->fitness = $this->vetDistancias[0];
            // SE O fitness FOR MAIOR QUE O CÁLCULO DAS DISTNCIAS fitness RECEBE O CÁLCULO
            $this->fitness = (float)($this->fitness > $this->vetDistancias[0] ? $this->vetDistancias[0] : $this->fitness);
        }

        echo "\n*** PROCESSO: #" . $this->idx . " -> FITNESS: {$this->getFitness()}\n";
        exit(1);
    }

    public function getFitness(): float
    {
        return $this->fitness;
    }

    /**
     * Retorna a distância total da viagem
     *
     * @return float
     */
    private function calcularDistancias(): float
    {
        // *** DA PRIMEIRA ATÉ A ÚLTIMA CIDADE ***
        // LEMBRANDO QUE A ÚLTIMA É UMA CÓPIA DA PRIMEIRA PARA GARANTIR O RETORNO
        $somaDistancias = 0;
        for ($l1 = 0; $l1 < count($this->vetCidades) - 1; $l1++)
            $somaDistancias += $this->vetCidades[$l1 + 1]->getDistancia($this->vetCidades[$l1]);

        return $somaDistancias;
    }

    /**
     * Faz a troca de posição das cidades
     */
    private function mutacao()
    {
        for ($x = 0; $x < $this->nInteracoes; $x++) {
            do { // NESTA REPETIÇÃO GARANTIMOS QUE $idxCidade1 NUNCA SERÁ IGUAL $idxCidade2
                $idxCidade1 = mt_rand(0, count($this->vetCidades) - 1);
                $idxCidade2 = mt_rand(0, count($this->vetCidades) - 1);
            } while ($idxCidade1 == $idxCidade2);

            // TROCANDO AS CIDADES DE POSICAO
            $auxTroca = $this->vetCidades[$idxCidade1];
            $this->vetCidades[$idxCidade1] = $this->vetCidades[$idxCidade2];
            $this->vetCidades[$idxCidade2] = $auxTroca;
        }
    }
}